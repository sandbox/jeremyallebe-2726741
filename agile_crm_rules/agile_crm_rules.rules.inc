<?php

module_load_include('inc', 'agile_crm', 'inc/curlwrap_v2');

function agile_crm_rules_rules_action_info() {
  $actions = array();

  $actions['agile_crm_rules_action_add_contact'] = array(
    'label' => t('Add a new contact to Agile CRM'),
    'group' => t('Agile CRM'),
    'parameter' => array(
      'mail' => array(
        'type' => 'text',
        'label' => t('Email'),
        'default mode' => 'selector',
      ),
      'firstname' => array(
        'type' => 'text',
        'label' => t('Firstname'),
        'default mode' => 'selector',
        'optional' => TRUE,
      ),
      'lastname' => array(
        'type' => 'text',
        'label' => t('Lastname'),
        'default mode' => 'selector',
        'optional' => TRUE,
      ),
      'picture' => array(
        'type' => 'text',
        'label' => t('Picture'),
        'default mode' => 'selector',
        'optional' => TRUE,
      ),
      'points' => array(
        'type' => 'text',
        'label' => t('Number of points'),
        'default mode' => 'input',
        'optional' => TRUE,
      ),
      'tags' => array(
        'type' => 'text',
        'label' => t('Tags (one by line)'),
        'default mode' => 'input',
        'optional' => TRUE,
      ),
    ),
    //      'callbacks' => array(
    //        'validate' => 'rules_action_custom_validation',
    //        'help' => 'rules_mail_help',
    //      ),
  );

  $actions['agile_crm_rules_action_update_contact'] = array(
    'label' => t('Update contact in Agile CRM'),
    'group' => t('Agile CRM'),
    'parameter' => array(
      'mail' => array(
        'type' => 'text',
        'label' => t('Email'),
        'default mode' => 'selector',
      ),
      'firstname' => array(
        'type' => 'text',
        'label' => t('Firstname'),
        'default mode' => 'selector',
        'optional' => TRUE,
      ),
      'lastname' => array(
        'type' => 'text',
        'label' => t('Lastname'),
        'default mode' => 'selector',
        'optional' => TRUE,
      ),
      'picture' => array(
        'type' => 'text',
        'label' => t('Picture'),
        'default mode' => 'selector',
        'optional' => TRUE,
      ),
    ),
  );

  $actions['agile_crm_rules_action_add_points_to_contact'] = array(
    'label' => t('Add points to a contact in Agile CRM'),
    'group' => t('Agile CRM'),
    'parameter' => array(
      'mail' => array(
        'type' => 'text',
        'label' => t('Email'),
        'default mode' => 'selector',
      ),
      'points' => array(
        'type' => 'text',
        'label' => t('Number of points'),
        'default mode' => 'input',
      ),
    ),
  );

  $actions['agile_crm_rules_action_add_tags_to_contact'] = array(
    'label' => t('Add tags to a contact in Agile CRM'),
    'group' => t('Agile CRM'),
    'parameter' => array(
      'mail' => array(
        'type' => 'text',
        'label' => t('Email'),
        'default mode' => 'selector',
      ),
      'tags' => array(
        'type' => 'text',
        'label' => t('Tags (one by line)'),
        'default mode' => 'input',
      ),
    ),
  );

  return $actions;
}



function agile_crm_rules_action_add_contact($mail, $firstname = "", $lastname = "", $image = "", $points = 0, $tags = ""){
  //  $address = array(
  //  "address"=>"Avenida Álvares Cabral 1777",
  //  "city"=>"Belo Horizonte",
  //  "state"=>"Minas Gerais",
  //  "country"=>"Brazil"
  //);

  $tags = explode("\n", $tags);
  $tags = array_filter($tags, 'trim');

  $contact_json = array(
    "lead_score" => $points,
    "star_value" => "5",
    "tags" => $tags,
    "properties"=>array(
      array(
        "name"=>"first_name",
        "value"=> ucwords($firstname),
        "type"=>"SYSTEM"
      ),
      array(
        "name"=>"last_name",
        "value"=> ucwords($lastname),
        "type"=>"SYSTEM"
      ),
      array(
        "name"=>"email",
        "value"=> $mail,
        "type"=>"SYSTEM"
      ),
      array(
        "name"=>"title",
        "value"=> "",
        "type"=>"SYSTEM"
      ),
      array(
        "name"=>"address",
        "value"=> "",
        "type"=>"SYSTEM"
      ),
      array(
        "name"=>"phone",
        "value"=> "",
        "type"=>"SYSTEM"
      ),
    )
  );

  $contact_json = json_encode($contact_json);
  $curl = agile_crm_curl_wrap("contacts", $contact_json, "POST", "application/json");
  watchdog('agilecrm', print_r($curl, true));

  return TRUE;
}


function agile_crm_rules_action_update_contact($mail, $firstname = "", $lastname = "", $image = ""){

  $u = agile_crm_curl_wrap("contacts/search/email/" . $mail, null, "GET", "application/json");
  $u = json_decode($u);

  if(isset($u)){
    $contact_json = array(
      "id" => $u->id,
      "properties"=>array(
        array(
          "name"=>"first_name",
          "value"=> ucwords($firstname),
          "type"=>"SYSTEM"
        ),
        array(
          "name"=>"last_name",
          "value"=> ucwords($lastname),
          "type"=>"SYSTEM"
        ),
        array(
          "name"=>"email",
          "value"=> $mail,
          "type"=>"SYSTEM"
        ),
      )
    );

    $contact_json = json_encode($contact_json);
    $curl = agile_crm_curl_wrap("contacts/edit-properties", $contact_json, "PUT", "application/json");
    watchdog('agilecrm', print_r($curl, true));

    return TRUE;
  }
}


function agile_crm_rules_action_add_points_to_contact($mail, $points){
  $u = agile_crm_curl_wrap("contacts/search/email/" . $mail, null, "GET", "application/json");
  $u = json_decode($u);

  if(isset($u)){
    $contact_json = array(
      "id" => $u->id, //It is mandatory field. Id of contact
      "lead_score" => $u->lead_score + $points
    );

    $contact_json = json_encode($contact_json);
    $curl = agile_crm_curl_wrap("contacts/edit/lead-score", $contact_json, "PUT", "application/json");
    watchdog('agilecrm', print_r($curl, true));

    return TRUE;
  }
}

function agile_crm_rules_action_add_tags_to_contact($mail, $tags){
  $tags = explode("\n", $tags);
  $tags = array_filter($tags, 'trim');

  if($tags > 0){
    $tags_formatted = '[';
    foreach ($tags as $line) {
      $line = str_replace("'", " ", $line);
      $line = str_replace(",", " ", $line);
      $tags_formatted .= '"' . trim($line) . '",';
    }

    $tags_formatted = rtrim($tags_formatted, ",");
    $tags_formatted .= ']';

    $fields = array(
      'email' => urlencode($mail),
      'tags' => urlencode($tags_formatted)
    );
    $fields_string = '';
    foreach ($fields as $key => $value) {
      $fields_string .= $key . '=' . $value . '&';
    }
    $curl = agile_crm_curl_wrap("contacts/email/tags/add", rtrim($fields_string, '&'), "POST", "application/x-www-form-urlencoded");
    watchdog('agilecrm', print_r($curl, true));
  }

  return TRUE;
}
