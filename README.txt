Provides integration with the Agile CRM service.

Installing
- Install this module in the normal Drupal way
- Sign-up for a free account on agilecrm.com
- Login to the dashboard
- Go to Admin Settings > Preferences
- Copy and paste the Domain and User Email
- Go to Admin Settings > API & Analytics
- Copy and paste the REST API key
